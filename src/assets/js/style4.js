var optionOpen = false;
var currUser = 0;
// Modification des elements pour le style 4
function changeToStyle4() {
    let elem = $('body').find('.main-nav');
    if (elem.length === 0) {
        // MENU
        // $('body').find('#user-display').remove().clone().insertBefore('h1');

        $(`<div class="nav-fixed"><div class="nav-container"><nav class="main-nav">
        <ul>
        <li class="logo"><a href="#">HSN</a></li>
        </ul>
        <button type="button" class="login">Connexion</button>
        </nav></div></div>`).insertBefore('h1');

        // Déplace déroulant themes
        elem = $('body').find('button.login');
        $('body').find('#themes').remove().clone().insertBefore(elem);


        // Déplace #add-user
        elem = $('body').find('.nav-fixed');
        $('#add-user').remove().clone().appendTo(elem);

        elem = $('#add-user');
        // H2
        elem.prepend(`<h2 class="new-compte">Créer un nouveau Compte</h2>`);
        elem.wrap(`<div class="login-container"></div>`);
        elem.append('<a href="#" class="login"><i class="fas fa-angle-down"></i></a>')

        elem = $('body').find('#user-card');
        $(elem).prepend(`<h2 class="abonne">Profil des Abonnés</h2>`);

        elem = $('body').find('#post-display');
        $(elem).prepend(`<button class="write-post" type="button">Nouveau Post<i class="fas fa-edit"></i></button>`);

        // Login
        elem = $('body').find('.login-container');
        $(elem).append(`
        <form action="#" method="get" id="login">
        <h2 class="connect">Se Connecter</h2>
        <input type="text" name="email" id="login-email" placeholder="Votre email*" />
        <input type="text" name="user-name" id="login-name" placeholder="Votre nom*" />
        <button type="submit">Connexion</button>
        </form>
        `)

        elem = $('#add-post');
        $(elem).addClass('wrap-post');
        $(elem).wrap( `<div class="add-post-container"></div>`);
        $(elem).prepend(`<button class="write-post" type="button"><i class="fas fa-edit"></i></button>`);

        elem = $('body').find('.option-container');
        $(elem).each(function() {
            $(this).append(`<i class="user-option fas fa-ellipsis-v"></i>`)
        });

        changeSelected("4");
    }
}

// LISTENER

$(document).on('change','#user-list', { passive: true }, function() {
    currUserOnline = $(this).val();
});

$(document).on('click','.write-post', { passive: true }, function() {
    $('.add-post-container').toggleClass("open")
})
$(document).on('click','.login', { passive: true }, function() {
    let div = $('body').find('.login-container');
    $(div).toggleClass("open")
})
$(document).on('click', function(event) {
    if (optionOpen) {
        if (event.target.id !== "#user-profil" &&
        !event.target.classList.contains('user-option')) {
            console.log('cllick');
            $('body').find('#user-profil').remove();
            $('body').find('i.open').toggleClass("open");
            optionOpen = false;
        }
    }

    if (event.target.id == "user-display" ||
    event.target.classList.contains('card-close')) {
        $('#user-display').toggleClass('open')
    }
})

$(document).on('click','.user-option', function() {
    if (!optionOpen) {
        $(this).parent().append(`<a href="" id="user-profil">Voir le Profil</a>`);
        optionOpen = true;
        $(this).toggleClass("open");
    }else {
        $('body').find('#user-profil').remove();
        $('body').find('i.open').toggleClass("open");
        optionOpen = false;
    }
})

$(document).on('click','#user-profil', function(event) {
    event.preventDefault();
    let intIndex = parseInt($(this).parent().attr('data-value'));
    let user = userData[intIndex];
    new UserDisplayer(user);
    $('#user-display').toggleClass('open');
})

function changeSelected(value) {
    $('body').find('#themes select').val(value);
}

function toggleUserOnline() {
    let avatar = $('body').find('.avatar');
    $(avatar).each(function() {

    })
}

function removeFromStyle4() {
    // Clean Menu
    let find = $('body').find('#user-display')
    $('#add-user').remove().clone().insertBefore(find);
    $('body').find('#themes').remove().clone().insertBefore('h1');

    // Clear elem add
    let elem = [".nav-fixed", "h2", ".write-post", ".login", ".user-option",".wrap", ".wrap-post"]
    for (var i = 0; i < elem.length; i++) {
        if (elem[i] != ".wrap" && elem[i] != ".wrap-post") {
            $('body').find(elem[i]).remove();
        }else {
            $('body').find(elem[i]).unwrap();
        }
    }

    changeSelected("1");

}
