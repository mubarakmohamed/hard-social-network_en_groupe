
/**
 * Le paramêtre `str` est-il un email valide ?
 * Retourne vrai si oui, faux sinon.
 *
 * /!\ Ce test est très pauvre...
 * Pour un test plus poussé, il faudrait utiliser une expression régulière (https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/RegExp)
 * de ce type https://www.regextester.com/19
 *
 * @param string str
 */
function isItAnEmail( str ) {
    return str.indexOf( '@' ) > -1;
}

function isItAString() {
    return true
}

/**
 * Le paramêtre `str` est-il un user name valide ?
 * Retourne vrai si oui, faux sinon.
 *
 * Pour plus d'explication sur cette expression régulière
 * https://stackoverflow.com/a/12019115
 *
 * @param string str
 */
function isItAUserName( str ) {
    let regExp = new RegExp( /^(?=.{3,20}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/ );
    return regExp.test( str );
}

/**
 * Le paramêtre `int` est-il un age ?
 * Retourne vrai si oui, faux sinon.
 *
 * @param int int
 */
function isItAnAge( int ) {
    return Number.isInteger( int ) && int > -1;
}

/**
 * Le paramêtre `user` est-il un User ?
 * Retourne vrai si oui, faux sinon.
 *
 * @param User user
 */
function isItAUser( user ) {
    return user instanceof User;
}

/**
 * Le paramêtre `post` est-il un Post ?
 * Retourne vrai si oui, faux sinon.
 *
 * @param Post user
 */
function isItAPost( post ) {
    return post instanceof Post;
}
